package org.javaboy.vhr.bean;

/**
 * 系统消息表
 */
public class SysMsg {
    /**
     * 编号
     */
    private Integer id;
    /**
     * 消息编号
     */
    private Integer mid;
    /**
     * 消息类型 0表示群发消息
     */
    private Integer type;
    /**
     * 这条消息发送给谁
     */
    private Integer hrid;
    /**
     * 状态 0未读:1已读
     */
    private Integer state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMid() {
        return mid;
    }

    public void setMid(Integer mid) {
        this.mid = mid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getHrid() {
        return hrid;
    }

    public void setHrid(Integer hrid) {
        this.hrid = hrid;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}